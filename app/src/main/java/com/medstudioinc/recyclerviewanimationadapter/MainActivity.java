package com.medstudioinc.recyclerviewanimationadapter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.studioinc.animation.recyclerview.adapters.ScaleInAnimationAdapter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    CustomAdapter adapter;
    ArrayList<String> strings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        strings = new ArrayList<>();

        for (int i = 0; i <= 5000; i++){
            strings.add("Item" + i);
        }
        adapter = new CustomAdapter(strings);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
        recyclerView.setAdapter(scaleInAnimationAdapter);
    }
}
