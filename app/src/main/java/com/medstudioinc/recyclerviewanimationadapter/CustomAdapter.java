package com.medstudioinc.recyclerviewanimationadapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyHolder>{
    private ArrayList<String> strings;

    public CustomAdapter(ArrayList<String> strings) {
        this.strings = strings;
    }


    public class MyHolder extends RecyclerView.ViewHolder{
        private TextView textView;
        public MyHolder(@NonNull View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.textView);
        }
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MyHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder myHolder, int i) {
        myHolder.textView.setText(strings.get(i));
    }


    @Override
    public int getItemCount() {
        return strings.size();
    }

}
