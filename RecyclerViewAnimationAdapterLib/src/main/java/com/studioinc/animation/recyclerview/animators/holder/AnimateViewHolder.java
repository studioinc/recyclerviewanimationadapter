//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.studioinc.animation.recyclerview.animators.holder;

import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.widget.RecyclerView.ViewHolder;

public interface AnimateViewHolder {
    void preAnimateAddImpl(ViewHolder var1);

    void preAnimateRemoveImpl(ViewHolder var1);

    void animateAddImpl(ViewHolder var1, ViewPropertyAnimatorListener var2);

    void animateRemoveImpl(ViewHolder var1, ViewPropertyAnimatorListener var2);
}
