//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.studioinc.animation.recyclerview.animators;

import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.animation.OvershootInterpolator;

public class OvershootInRightAnimator extends BaseItemAnimator {
    private final float mTension;

    public OvershootInRightAnimator() {
        this.mTension = 2.0F;
    }

    public OvershootInRightAnimator(float mTension) {
        this.mTension = mTension;
    }

    protected void animateRemoveImpl(ViewHolder holder) {
        ViewCompat.animate(holder.itemView).translationX((float)holder.itemView.getRootView().getWidth()).setDuration(this.getRemoveDuration()).setListener(new DefaultRemoveVpaListener(holder)).setStartDelay(this.getRemoveDelay(holder)).start();
    }

    protected void preAnimateAddImpl(ViewHolder holder) {
        ViewCompat.setTranslationX(holder.itemView, (float)holder.itemView.getRootView().getWidth());
    }

    protected void animateAddImpl(ViewHolder holder) {
        ViewCompat.animate(holder.itemView).translationX(0.0F).setDuration(this.getAddDuration()).setInterpolator(new OvershootInterpolator(this.mTension)).setListener(new DefaultAddVpaListener(holder)).setStartDelay(this.getAddDelay(holder)).start();
    }
}
