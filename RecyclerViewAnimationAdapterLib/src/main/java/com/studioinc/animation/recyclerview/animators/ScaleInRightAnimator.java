//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.studioinc.animation.recyclerview.animators;

import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.animation.Interpolator;

public class ScaleInRightAnimator extends BaseItemAnimator {
    public ScaleInRightAnimator() {
    }

    public ScaleInRightAnimator(Interpolator interpolator) {
        this.mInterpolator = interpolator;
    }

    protected void preAnimateRemoveImpl(ViewHolder holder) {
        ViewCompat.setPivotX(holder.itemView, (float)holder.itemView.getWidth());
    }

    protected void animateRemoveImpl(ViewHolder holder) {
        ViewCompat.animate(holder.itemView).scaleX(0.0F).scaleY(0.0F).setDuration(this.getRemoveDuration()).setInterpolator(this.mInterpolator).setListener(new DefaultRemoveVpaListener(holder)).setStartDelay(this.getRemoveDelay(holder)).start();
    }

    protected void preAnimateAddImpl(ViewHolder holder) {
        ViewCompat.setPivotX(holder.itemView, (float)holder.itemView.getWidth());
        ViewCompat.setScaleX(holder.itemView, 0.0F);
        ViewCompat.setScaleY(holder.itemView, 0.0F);
    }

    protected void animateAddImpl(ViewHolder holder) {
        ViewCompat.animate(holder.itemView).scaleX(1.0F).scaleY(1.0F).setDuration(this.getAddDuration()).setInterpolator(this.mInterpolator).setListener(new DefaultAddVpaListener(holder)).setStartDelay(this.getAddDelay(holder)).start();
    }
}
