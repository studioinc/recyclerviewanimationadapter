//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.studioinc.animation.recyclerview.animators;

import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.animation.OvershootInterpolator;

public class OvershootInLeftAnimator extends BaseItemAnimator {
    private final float mTension;

    public OvershootInLeftAnimator() {
        this.mTension = 2.0F;
    }

    public OvershootInLeftAnimator(float mTension) {
        this.mTension = mTension;
    }

    protected void animateRemoveImpl(ViewHolder holder) {
        ViewCompat.animate(holder.itemView).translationX((float)(-holder.itemView.getRootView().getWidth())).setDuration(this.getRemoveDuration()).setListener(new DefaultRemoveVpaListener(holder)).setStartDelay(this.getRemoveDelay(holder)).start();
    }

    protected void preAnimateAddImpl(ViewHolder holder) {
        ViewCompat.setTranslationX(holder.itemView, (float)(-holder.itemView.getRootView().getWidth()));
    }

    protected void animateAddImpl(ViewHolder holder) {
        ViewCompat.animate(holder.itemView).translationX(0.0F).setDuration(this.getAddDuration()).setListener(new DefaultAddVpaListener(holder)).setInterpolator(new OvershootInterpolator(this.mTension)).setStartDelay(this.getAddDelay(holder)).start();
    }
}
