package com.studioinc.animation.recyclerview.adapters;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;

public class SlideInLeftAnimationAdapter extends AnimationAdapter {
    public SlideInLeftAnimationAdapter(Adapter adapter) {
        super(adapter);
    }

    protected Animator[] getAnimators(View view) {
        return new Animator[]{ObjectAnimator.ofFloat(view, "translationX", new float[]{(float)(-view.getRootView().getWidth()), 0.0F})};
    }
}