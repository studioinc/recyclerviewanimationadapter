package com.studioinc.animation.recyclerview.adapters;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;

public class SlideInBottomAnimationAdapter extends AnimationAdapter {
    public SlideInBottomAnimationAdapter(Adapter adapter) {
        super(adapter);
    }

    protected Animator[] getAnimators(View view) {
        return new Animator[]{ObjectAnimator.ofFloat(view, "translationY", new float[]{(float)view.getMeasuredHeight(), 0.0F})};
    }
}
