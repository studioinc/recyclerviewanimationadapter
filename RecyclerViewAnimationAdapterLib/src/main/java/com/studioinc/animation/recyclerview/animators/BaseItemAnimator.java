//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.studioinc.animation.recyclerview.animators;

import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

import com.studioinc.animation.recyclerview.animators.holder.AnimateViewHolder;
import com.studioinc.animation.recyclerview.internal.ViewHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public abstract class BaseItemAnimator extends SimpleItemAnimator {
    private static final boolean DEBUG = false;
    private ArrayList<ViewHolder> mPendingRemovals = new ArrayList();
    private ArrayList<ViewHolder> mPendingAdditions = new ArrayList();
    private ArrayList<MoveInfo> mPendingMoves = new ArrayList();
    private ArrayList<ChangeInfo> mPendingChanges = new ArrayList();
    private ArrayList<ArrayList<ViewHolder>> mAdditionsList = new ArrayList();
    private ArrayList<ArrayList<MoveInfo>> mMovesList = new ArrayList();
    private ArrayList<ArrayList<ChangeInfo>> mChangesList = new ArrayList();
    protected ArrayList<ViewHolder> mAddAnimations = new ArrayList();
    private ArrayList<ViewHolder> mMoveAnimations = new ArrayList();
    protected ArrayList<ViewHolder> mRemoveAnimations = new ArrayList();
    private ArrayList<ViewHolder> mChangeAnimations = new ArrayList();
    protected Interpolator mInterpolator = new DecelerateInterpolator();

    public BaseItemAnimator() {
        this.setSupportsChangeAnimations(false);
    }

    public void setInterpolator(Interpolator mInterpolator) {
        this.mInterpolator = mInterpolator;
    }

    public void runPendingAnimations() {
        boolean removalsPending = !this.mPendingRemovals.isEmpty();
        boolean movesPending = !this.mPendingMoves.isEmpty();
        boolean changesPending = !this.mPendingChanges.isEmpty();
        boolean additionsPending = !this.mPendingAdditions.isEmpty();
        if (removalsPending || movesPending || additionsPending || changesPending) {
            Iterator var5 = this.mPendingRemovals.iterator();

            while(var5.hasNext()) {
                ViewHolder holder = (ViewHolder)var5.next();
                this.doAnimateRemove(holder);
            }

            this.mPendingRemovals.clear();
            ArrayList additions;
            Runnable adder;
            if (movesPending) {
                additions = new ArrayList();
                additions.addAll(this.mPendingMoves);
                this.mMovesList.add(additions);
                this.mPendingMoves.clear();
                final ArrayList finalAdditions2 = additions;
                adder = new Runnable() {
                    public void run() {
                        boolean removed = BaseItemAnimator.this.mMovesList.remove(finalAdditions2);
                        if (removed) {
                            Iterator var2 = finalAdditions2.iterator();

                            while(var2.hasNext()) {
                                BaseItemAnimator.MoveInfo moveInfo = (BaseItemAnimator.MoveInfo)var2.next();
                                BaseItemAnimator.this.animateMoveImpl(moveInfo.holder, moveInfo.fromX, moveInfo.fromY, moveInfo.toX, moveInfo.toY);
                            }

                            finalAdditions2.clear();
                        }
                    }
                };
                if (removalsPending) {
                    View view = ((BaseItemAnimator.MoveInfo)additions.get(0)).holder.itemView;
                    ViewCompat.postOnAnimationDelayed(view, adder, this.getRemoveDuration());
                } else {
                    adder.run();
                }
            }

            if (changesPending) {
                additions = new ArrayList();
                additions.addAll(this.mPendingChanges);
                this.mChangesList.add(additions);
                this.mPendingChanges.clear();
                final ArrayList finalAdditions1 = additions;
                adder = new Runnable() {
                    public void run() {
                        boolean removed = BaseItemAnimator.this.mChangesList.remove(finalAdditions1);
                        if (removed) {
                            Iterator var2 = finalAdditions1.iterator();

                            while(var2.hasNext()) {
                                BaseItemAnimator.ChangeInfo change = (BaseItemAnimator.ChangeInfo)var2.next();
                                BaseItemAnimator.this.animateChangeImpl(change);
                            }

                            finalAdditions1.clear();
                        }
                    }
                };
                if (removalsPending) {
                    ViewHolder holder = ((BaseItemAnimator.ChangeInfo)additions.get(0)).oldHolder;
                    ViewCompat.postOnAnimationDelayed(holder.itemView, adder, this.getRemoveDuration());
                } else {
                    adder.run();
                }
            }

            if (additionsPending) {
                additions = new ArrayList();
                additions.addAll(this.mPendingAdditions);
                this.mAdditionsList.add(additions);
                this.mPendingAdditions.clear();
                final ArrayList finalAdditions = additions;
                adder = new Runnable() {
                    public void run() {
                        boolean removed = BaseItemAnimator.this.mAdditionsList.remove(finalAdditions);
                        if (removed) {
                            Iterator var2 = finalAdditions.iterator();

                            while(var2.hasNext()) {
                                ViewHolder holder = (ViewHolder)var2.next();
                                BaseItemAnimator.this.doAnimateAdd(holder);
                            }

                            finalAdditions.clear();
                        }
                    }
                };
                if (!removalsPending && !movesPending && !changesPending) {
                    adder.run();
                } else {
                    long removeDuration = removalsPending ? this.getRemoveDuration() : 0L;
                    long moveDuration = movesPending ? this.getMoveDuration() : 0L;
                    long changeDuration = changesPending ? this.getChangeDuration() : 0L;
                    long totalDelay = removeDuration + Math.max(moveDuration, changeDuration);
                    View view = ((ViewHolder)additions.get(0)).itemView;
                    ViewCompat.postOnAnimationDelayed(view, adder, totalDelay);
                }
            }

        }
    }

    protected void preAnimateRemoveImpl(ViewHolder holder) {
    }

    protected void preAnimateAddImpl(ViewHolder holder) {
    }

    protected abstract void animateRemoveImpl(ViewHolder var1);

    protected abstract void animateAddImpl(ViewHolder var1);

    private void preAnimateRemove(ViewHolder holder) {
        ViewHelper.clear(holder.itemView);
        if (holder instanceof AnimateViewHolder) {
            ((AnimateViewHolder)holder).preAnimateRemoveImpl(holder);
        } else {
            this.preAnimateRemoveImpl(holder);
        }

    }

    private void preAnimateAdd(ViewHolder holder) {
        ViewHelper.clear(holder.itemView);
        if (holder instanceof AnimateViewHolder) {
            ((AnimateViewHolder)holder).preAnimateAddImpl(holder);
        } else {
            this.preAnimateAddImpl(holder);
        }

    }

    private void doAnimateRemove(ViewHolder holder) {
        if (holder instanceof AnimateViewHolder) {
            ((AnimateViewHolder)holder).animateRemoveImpl(holder, new BaseItemAnimator.DefaultRemoveVpaListener(holder));
        } else {
            this.animateRemoveImpl(holder);
        }

        this.mRemoveAnimations.add(holder);
    }

    private void doAnimateAdd(ViewHolder holder) {
        if (holder instanceof AnimateViewHolder) {
            ((AnimateViewHolder)holder).animateAddImpl(holder, new BaseItemAnimator.DefaultAddVpaListener(holder));
        } else {
            this.animateAddImpl(holder);
        }

        this.mAddAnimations.add(holder);
    }

    public boolean animateRemove(ViewHolder holder) {
        this.endAnimation(holder);
        this.preAnimateRemove(holder);
        this.mPendingRemovals.add(holder);
        return true;
    }

    protected long getRemoveDelay(ViewHolder holder) {
        return Math.abs((long)holder.getOldPosition() * this.getRemoveDuration() / 4L);
    }

    public boolean animateAdd(ViewHolder holder) {
        this.endAnimation(holder);
        this.preAnimateAdd(holder);
        this.mPendingAdditions.add(holder);
        return true;
    }

    protected long getAddDelay(ViewHolder holder) {
        return Math.abs((long)holder.getAdapterPosition() * this.getAddDuration() / 4L);
    }

    public boolean animateMove(ViewHolder holder, int fromX, int fromY, int toX, int toY) {
        View view = holder.itemView;
        fromX = (int)((float)fromX + ViewCompat.getTranslationX(holder.itemView));
        fromY = (int)((float)fromY + ViewCompat.getTranslationY(holder.itemView));
        this.endAnimation(holder);
        int deltaX = toX - fromX;
        int deltaY = toY - fromY;
        if (deltaX == 0 && deltaY == 0) {
            this.dispatchMoveFinished(holder);
            return false;
        } else {
            if (deltaX != 0) {
                ViewCompat.setTranslationX(view, (float)(-deltaX));
            }

            if (deltaY != 0) {
                ViewCompat.setTranslationY(view, (float)(-deltaY));
            }

            this.mPendingMoves.add(new BaseItemAnimator.MoveInfo(holder, fromX, fromY, toX, toY));
            return true;
        }
    }

    private void animateMoveImpl(final ViewHolder holder, int fromX, int fromY, int toX, int toY) {
        View view = holder.itemView;
        final int deltaX = toX - fromX;
        final int deltaY = toY - fromY;
        if (deltaX != 0) {
            ViewCompat.animate(view).translationX(0.0F);
        }

        if (deltaY != 0) {
            ViewCompat.animate(view).translationY(0.0F);
        }

        this.mMoveAnimations.add(holder);
        final ViewPropertyAnimatorCompat animation = ViewCompat.animate(view);
        animation.setDuration(this.getMoveDuration()).setListener(new BaseItemAnimator.VpaListenerAdapter() {
            public void onAnimationStart(View view) {
                BaseItemAnimator.this.dispatchMoveStarting(holder);
            }

            public void onAnimationCancel(View view) {
                if (deltaX != 0) {
                    ViewCompat.setTranslationX(view, 0.0F);
                }

                if (deltaY != 0) {
                    ViewCompat.setTranslationY(view, 0.0F);
                }

            }

            public void onAnimationEnd(View view) {
                animation.setListener((ViewPropertyAnimatorListener)null);
                BaseItemAnimator.this.dispatchMoveFinished(holder);
                BaseItemAnimator.this.mMoveAnimations.remove(holder);
                BaseItemAnimator.this.dispatchFinishedWhenDone();
            }
        }).start();
    }

    public boolean animateChange(ViewHolder oldHolder, ViewHolder newHolder, int fromX, int fromY, int toX, int toY) {
        float prevTranslationX = ViewCompat.getTranslationX(oldHolder.itemView);
        float prevTranslationY = ViewCompat.getTranslationY(oldHolder.itemView);
        float prevAlpha = ViewCompat.getAlpha(oldHolder.itemView);
        this.endAnimation(oldHolder);
        int deltaX = (int)((float)(toX - fromX) - prevTranslationX);
        int deltaY = (int)((float)(toY - fromY) - prevTranslationY);
        ViewCompat.setTranslationX(oldHolder.itemView, prevTranslationX);
        ViewCompat.setTranslationY(oldHolder.itemView, prevTranslationY);
        ViewCompat.setAlpha(oldHolder.itemView, prevAlpha);
        if (newHolder != null && newHolder.itemView != null) {
            this.endAnimation(newHolder);
            ViewCompat.setTranslationX(newHolder.itemView, (float)(-deltaX));
            ViewCompat.setTranslationY(newHolder.itemView, (float)(-deltaY));
            ViewCompat.setAlpha(newHolder.itemView, 0.0F);
        }

        this.mPendingChanges.add(new BaseItemAnimator.ChangeInfo(oldHolder, newHolder, fromX, fromY, toX, toY));
        return true;
    }

    private void animateChangeImpl(final BaseItemAnimator.ChangeInfo changeInfo) {
        ViewHolder holder = changeInfo.oldHolder;
        View view = holder == null ? null : holder.itemView;
        ViewHolder newHolder = changeInfo.newHolder;
        final View newView = newHolder != null ? newHolder.itemView : null;
        ViewPropertyAnimatorCompat newViewAnimation;
        if (view != null) {
            this.mChangeAnimations.add(changeInfo.oldHolder);
            newViewAnimation = ViewCompat.animate(view).setDuration(this.getChangeDuration());
            newViewAnimation.translationX((float)(changeInfo.toX - changeInfo.fromX));
            newViewAnimation.translationY((float)(changeInfo.toY - changeInfo.fromY));
            final ViewPropertyAnimatorCompat finalNewViewAnimation1 = newViewAnimation;
            newViewAnimation.alpha(0.0F).setListener(new BaseItemAnimator.VpaListenerAdapter() {
                public void onAnimationStart(View view) {
                    BaseItemAnimator.this.dispatchChangeStarting(changeInfo.oldHolder, true);
                }

                public void onAnimationEnd(View view) {
                    finalNewViewAnimation1.setListener((ViewPropertyAnimatorListener)null);
                    ViewCompat.setAlpha(view, 1.0F);
                    ViewCompat.setTranslationX(view, 0.0F);
                    ViewCompat.setTranslationY(view, 0.0F);
                    BaseItemAnimator.this.dispatchChangeFinished(changeInfo.oldHolder, true);
                    BaseItemAnimator.this.mChangeAnimations.remove(changeInfo.oldHolder);
                    BaseItemAnimator.this.dispatchFinishedWhenDone();
                }
            }).start();
        }

        if (newView != null) {
            this.mChangeAnimations.add(changeInfo.newHolder);
            newViewAnimation = ViewCompat.animate(newView);
            final ViewPropertyAnimatorCompat finalNewViewAnimation = newViewAnimation;
            newViewAnimation.translationX(0.0F).translationY(0.0F).setDuration(this.getChangeDuration()).alpha(1.0F).setListener(new BaseItemAnimator.VpaListenerAdapter() {
                public void onAnimationStart(View view) {
                    BaseItemAnimator.this.dispatchChangeStarting(changeInfo.newHolder, false);
                }

                public void onAnimationEnd(View view) {
                    finalNewViewAnimation.setListener((ViewPropertyAnimatorListener)null);
                    ViewCompat.setAlpha(newView, 1.0F);
                    ViewCompat.setTranslationX(newView, 0.0F);
                    ViewCompat.setTranslationY(newView, 0.0F);
                    BaseItemAnimator.this.dispatchChangeFinished(changeInfo.newHolder, false);
                    BaseItemAnimator.this.mChangeAnimations.remove(changeInfo.newHolder);
                    BaseItemAnimator.this.dispatchFinishedWhenDone();
                }
            }).start();
        }

    }

    private void endChangeAnimation(List<ChangeInfo> infoList, ViewHolder item) {
        for(int i = infoList.size() - 1; i >= 0; --i) {
            BaseItemAnimator.ChangeInfo changeInfo = (BaseItemAnimator.ChangeInfo)infoList.get(i);
            if (this.endChangeAnimationIfNecessary(changeInfo, item) && changeInfo.oldHolder == null && changeInfo.newHolder == null) {
                infoList.remove(changeInfo);
            }
        }

    }

    private void endChangeAnimationIfNecessary(BaseItemAnimator.ChangeInfo changeInfo) {
        if (changeInfo.oldHolder != null) {
            this.endChangeAnimationIfNecessary(changeInfo, changeInfo.oldHolder);
        }

        if (changeInfo.newHolder != null) {
            this.endChangeAnimationIfNecessary(changeInfo, changeInfo.newHolder);
        }

    }

    private boolean endChangeAnimationIfNecessary(BaseItemAnimator.ChangeInfo changeInfo, ViewHolder item) {
        boolean oldItem = false;
        if (changeInfo.newHolder == item) {
            changeInfo.newHolder = null;
        } else {
            if (changeInfo.oldHolder != item) {
                return false;
            }

            changeInfo.oldHolder = null;
            oldItem = true;
        }

        ViewCompat.setAlpha(item.itemView, 1.0F);
        ViewCompat.setTranslationX(item.itemView, 0.0F);
        ViewCompat.setTranslationY(item.itemView, 0.0F);
        this.dispatchChangeFinished(item, oldItem);
        return true;
    }

    public void endAnimation(ViewHolder item) {
        View view = item.itemView;
        ViewCompat.animate(view).cancel();

        int i;
        for(i = this.mPendingMoves.size() - 1; i >= 0; --i) {
            BaseItemAnimator.MoveInfo moveInfo = (BaseItemAnimator.MoveInfo)this.mPendingMoves.get(i);
            if (moveInfo.holder == item) {
                ViewCompat.setTranslationY(view, 0.0F);
                ViewCompat.setTranslationX(view, 0.0F);
                this.dispatchMoveFinished(item);
                this.mPendingMoves.remove(i);
            }
        }

        this.endChangeAnimation(this.mPendingChanges, item);
        if (this.mPendingRemovals.remove(item)) {
            ViewHelper.clear(item.itemView);
            this.dispatchRemoveFinished(item);
        }

        if (this.mPendingAdditions.remove(item)) {
            ViewHelper.clear(item.itemView);
            this.dispatchAddFinished(item);
        }

        ArrayList moves;
        for(i = this.mChangesList.size() - 1; i >= 0; --i) {
            moves = (ArrayList)this.mChangesList.get(i);
            this.endChangeAnimation(moves, item);
            if (moves.isEmpty()) {
                this.mChangesList.remove(i);
            }
        }

        for(i = this.mMovesList.size() - 1; i >= 0; --i) {
            moves = (ArrayList)this.mMovesList.get(i);

            for(int j = moves.size() - 1; j >= 0; --j) {
                BaseItemAnimator.MoveInfo moveInfo = (BaseItemAnimator.MoveInfo)moves.get(j);
                if (moveInfo.holder == item) {
                    ViewCompat.setTranslationY(view, 0.0F);
                    ViewCompat.setTranslationX(view, 0.0F);
                    this.dispatchMoveFinished(item);
                    moves.remove(j);
                    if (moves.isEmpty()) {
                        this.mMovesList.remove(i);
                    }
                    break;
                }
            }
        }

        for(i = this.mAdditionsList.size() - 1; i >= 0; --i) {
            moves = (ArrayList)this.mAdditionsList.get(i);
            if (moves.remove(item)) {
                ViewHelper.clear(item.itemView);
                this.dispatchAddFinished(item);
                if (moves.isEmpty()) {
                    this.mAdditionsList.remove(i);
                }
            }
        }

        if (this.mRemoveAnimations.remove(item)) {
            ;
        }

        if (this.mAddAnimations.remove(item)) {
            ;
        }

        if (this.mChangeAnimations.remove(item)) {
            ;
        }

        if (this.mMoveAnimations.remove(item)) {
            ;
        }

        this.dispatchFinishedWhenDone();
    }

    public boolean isRunning() {
        return !this.mPendingAdditions.isEmpty() || !this.mPendingChanges.isEmpty() || !this.mPendingMoves.isEmpty() || !this.mPendingRemovals.isEmpty() || !this.mMoveAnimations.isEmpty() || !this.mRemoveAnimations.isEmpty() || !this.mAddAnimations.isEmpty() || !this.mChangeAnimations.isEmpty() || !this.mMovesList.isEmpty() || !this.mAdditionsList.isEmpty() || !this.mChangesList.isEmpty();
    }

    private void dispatchFinishedWhenDone() {
        if (!this.isRunning()) {
            this.dispatchAnimationsFinished();
        }

    }

    public void endAnimations() {
        int count = this.mPendingMoves.size();

        int listCount;
        for(listCount = count - 1; listCount >= 0; --listCount) {
            BaseItemAnimator.MoveInfo item = (BaseItemAnimator.MoveInfo)this.mPendingMoves.get(listCount);
            View view = item.holder.itemView;
            ViewCompat.setTranslationY(view, 0.0F);
            ViewCompat.setTranslationX(view, 0.0F);
            this.dispatchMoveFinished(item.holder);
            this.mPendingMoves.remove(listCount);
        }

        count = this.mPendingRemovals.size();

        ViewHolder item;
        for(listCount = count - 1; listCount >= 0; --listCount) {
            item = (ViewHolder)this.mPendingRemovals.get(listCount);
            this.dispatchRemoveFinished(item);
            this.mPendingRemovals.remove(listCount);
        }

        count = this.mPendingAdditions.size();

        for(listCount = count - 1; listCount >= 0; --listCount) {
            item = (ViewHolder)this.mPendingAdditions.get(listCount);
            ViewHelper.clear(item.itemView);
            this.dispatchAddFinished(item);
            this.mPendingAdditions.remove(listCount);
        }

        count = this.mPendingChanges.size();

        for(listCount = count - 1; listCount >= 0; --listCount) {
            this.endChangeAnimationIfNecessary((BaseItemAnimator.ChangeInfo)this.mPendingChanges.get(listCount));
        }

        this.mPendingChanges.clear();
        if (this.isRunning()) {
            listCount = this.mMovesList.size();

            int j;
            int i;
            ArrayList changes;
            for(i = listCount - 1; i >= 0; --i) {
                changes = (ArrayList)this.mMovesList.get(i);
                count = changes.size();

                for(j = count - 1; j >= 0; --j) {
                    BaseItemAnimator.MoveInfo moveInfo = (BaseItemAnimator.MoveInfo)changes.get(j);
                    ViewHolder items = moveInfo.holder;
                    View view = items.itemView;
                    ViewCompat.setTranslationY(view, 0.0F);
                    ViewCompat.setTranslationX(view, 0.0F);
                    this.dispatchMoveFinished(moveInfo.holder);
                    changes.remove(j);
                    if (changes.isEmpty()) {
                        this.mMovesList.remove(changes);
                    }
                }
            }

            listCount = this.mAdditionsList.size();

            for(i = listCount - 1; i >= 0; --i) {
                changes = (ArrayList)this.mAdditionsList.get(i);
                count = changes.size();

                for(j = count - 1; j >= 0; --j) {
                    ViewHolder items = (ViewHolder)changes.get(j);
                    View view = items.itemView;
                    ViewCompat.setAlpha(view, 1.0F);
                    this.dispatchAddFinished(items);
                    if (j < changes.size()) {
                        changes.remove(j);
                    }

                    if (changes.isEmpty()) {
                        this.mAdditionsList.remove(changes);
                    }
                }
            }

            listCount = this.mChangesList.size();

            for(i = listCount - 1; i >= 0; --i) {
                changes = (ArrayList)this.mChangesList.get(i);
                count = changes.size();

                for(j = count - 1; j >= 0; --j) {
                    this.endChangeAnimationIfNecessary((BaseItemAnimator.ChangeInfo)changes.get(j));
                    if (changes.isEmpty()) {
                        this.mChangesList.remove(changes);
                    }
                }
            }

            this.cancelAll(this.mRemoveAnimations);
            this.cancelAll(this.mMoveAnimations);
            this.cancelAll(this.mAddAnimations);
            this.cancelAll(this.mChangeAnimations);
            this.dispatchAnimationsFinished();
        }
    }

    void cancelAll(List<ViewHolder> viewHolders) {
        for(int i = viewHolders.size() - 1; i >= 0; --i) {
            ViewCompat.animate(((ViewHolder)viewHolders.get(i)).itemView).cancel();
        }

    }

    protected class DefaultRemoveVpaListener extends BaseItemAnimator.VpaListenerAdapter {
        ViewHolder mViewHolder;

        public DefaultRemoveVpaListener(ViewHolder holder) {
            super();
            this.mViewHolder = holder;
        }

        public void onAnimationStart(View view) {
            BaseItemAnimator.this.dispatchRemoveStarting(this.mViewHolder);
        }

        public void onAnimationCancel(View view) {
            ViewHelper.clear(view);
        }

        public void onAnimationEnd(View view) {
            ViewHelper.clear(view);
            BaseItemAnimator.this.dispatchRemoveFinished(this.mViewHolder);
            BaseItemAnimator.this.mRemoveAnimations.remove(this.mViewHolder);
            BaseItemAnimator.this.dispatchFinishedWhenDone();
        }
    }

    protected class DefaultAddVpaListener extends BaseItemAnimator.VpaListenerAdapter {
        ViewHolder mViewHolder;

        public DefaultAddVpaListener(ViewHolder holder) {
            super();
            this.mViewHolder = holder;
        }

        public void onAnimationStart(View view) {
            BaseItemAnimator.this.dispatchAddStarting(this.mViewHolder);
        }

        public void onAnimationCancel(View view) {
            ViewHelper.clear(view);
        }

        public void onAnimationEnd(View view) {
            ViewHelper.clear(view);
            BaseItemAnimator.this.dispatchAddFinished(this.mViewHolder);
            BaseItemAnimator.this.mAddAnimations.remove(this.mViewHolder);
            BaseItemAnimator.this.dispatchFinishedWhenDone();
        }
    }

    private static class VpaListenerAdapter implements ViewPropertyAnimatorListener {
        private VpaListenerAdapter() {
        }

        public void onAnimationStart(View view) {
        }

        public void onAnimationEnd(View view) {
        }

        public void onAnimationCancel(View view) {
        }
    }

    private static class ChangeInfo {
        public ViewHolder oldHolder;
        public ViewHolder newHolder;
        public int fromX;
        public int fromY;
        public int toX;
        public int toY;

        private ChangeInfo(ViewHolder oldHolder, ViewHolder newHolder) {
            this.oldHolder = oldHolder;
            this.newHolder = newHolder;
        }

        private ChangeInfo(ViewHolder oldHolder, ViewHolder newHolder, int fromX, int fromY, int toX, int toY) {
            this(oldHolder, newHolder);
            this.fromX = fromX;
            this.fromY = fromY;
            this.toX = toX;
            this.toY = toY;
        }

        public String toString() {
            return "ChangeInfo{oldHolder=" + this.oldHolder + ", newHolder=" + this.newHolder + ", fromX=" + this.fromX + ", fromY=" + this.fromY + ", toX=" + this.toX + ", toY=" + this.toY + '}';
        }
    }

    private static class MoveInfo {
        public ViewHolder holder;
        public int fromX;
        public int fromY;
        public int toX;
        public int toY;

        private MoveInfo(ViewHolder holder, int fromX, int fromY, int toX, int toY) {
            this.holder = holder;
            this.fromX = fromX;
            this.fromY = fromY;
            this.toX = toX;
            this.toY = toY;
        }
    }
}
