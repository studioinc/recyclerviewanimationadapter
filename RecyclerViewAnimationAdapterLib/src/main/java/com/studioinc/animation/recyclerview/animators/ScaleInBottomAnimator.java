//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.studioinc.animation.recyclerview.animators;

import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.animation.Interpolator;

public class ScaleInBottomAnimator extends BaseItemAnimator {
    public ScaleInBottomAnimator() {
    }

    public ScaleInBottomAnimator(Interpolator interpolator) {
        this.mInterpolator = interpolator;
    }

    protected void preAnimateRemoveImpl(ViewHolder holder) {
        holder.itemView.setPivotY((float)holder.itemView.getHeight());
    }

    protected void animateRemoveImpl(ViewHolder holder) {
        ViewCompat.animate(holder.itemView).scaleX(0.0F).scaleY(0.0F).setDuration(this.getRemoveDuration()).setInterpolator(this.mInterpolator).setListener(new DefaultRemoveVpaListener(holder)).setStartDelay(this.getRemoveDelay(holder)).start();
    }

    protected void preAnimateAddImpl(ViewHolder holder) {
        holder.itemView.setPivotY((float)holder.itemView.getHeight());
        ViewCompat.setScaleX(holder.itemView, 0.0F);
        ViewCompat.setScaleY(holder.itemView, 0.0F);
    }

    protected void animateAddImpl(ViewHolder holder) {
        ViewCompat.animate(holder.itemView).scaleX(1.0F).scaleY(1.0F).setDuration(this.getAddDuration()).setInterpolator(this.mInterpolator).setListener(new DefaultAddVpaListener(holder)).setStartDelay(this.getAddDelay(holder)).start();
    }
}
