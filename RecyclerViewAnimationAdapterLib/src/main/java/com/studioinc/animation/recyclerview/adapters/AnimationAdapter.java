package com.studioinc.animation.recyclerview.adapters;

import android.animation.Animator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.AdapterDataObserver;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import com.studioinc.animation.recyclerview.internal.ViewHelper;


public abstract class AnimationAdapter extends Adapter<ViewHolder> {
    private Adapter<ViewHolder> mAdapter;
    private int mDuration = 300;
    private Interpolator mInterpolator = new LinearInterpolator();
    private int mLastPosition = -1;
    private boolean isFirstOnly = true;

    public AnimationAdapter(Adapter<ViewHolder> adapter) {
        this.mAdapter = adapter;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return this.mAdapter.onCreateViewHolder(parent, viewType);
    }

    public void registerAdapterDataObserver(AdapterDataObserver observer) {
        super.registerAdapterDataObserver(observer);
        this.mAdapter.registerAdapterDataObserver(observer);
    }

    public void unregisterAdapterDataObserver(AdapterDataObserver observer) {
        super.unregisterAdapterDataObserver(observer);
        this.mAdapter.unregisterAdapterDataObserver(observer);
    }

    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.mAdapter.onAttachedToRecyclerView(recyclerView);
    }

    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        this.mAdapter.onDetachedFromRecyclerView(recyclerView);
    }

    public void onViewAttachedToWindow(ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        this.mAdapter.onViewAttachedToWindow(holder);
    }

    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        this.mAdapter.onViewDetachedFromWindow(holder);
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        this.mAdapter.onBindViewHolder(holder, position);
        int adapterPosition = holder.getAdapterPosition();
        if (this.isFirstOnly && adapterPosition <= this.mLastPosition) {
            ViewHelper.clear(holder.itemView);
        } else {
            Animator[] var4 = this.getAnimators(holder.itemView);
            int var5 = var4.length;

            for(int var6 = 0; var6 < var5; ++var6) {
                Animator anim = var4[var6];
                anim.setDuration((long)this.mDuration).start();
                anim.setInterpolator(this.mInterpolator);
            }

            this.mLastPosition = adapterPosition;
        }

    }

    public void onViewRecycled(ViewHolder holder) {
        this.mAdapter.onViewRecycled(holder);
        super.onViewRecycled(holder);
    }

    public int getItemCount() {
        return this.mAdapter.getItemCount();
    }

    public void setDuration(int duration) {
        this.mDuration = duration;
    }

    public void setInterpolator(Interpolator interpolator) {
        this.mInterpolator = interpolator;
    }

    public void setStartPosition(int start) {
        this.mLastPosition = start;
    }

    protected abstract Animator[] getAnimators(View var1);

    public void setFirstOnly(boolean firstOnly) {
        this.isFirstOnly = firstOnly;
    }

    public int getItemViewType(int position) {
        return this.mAdapter.getItemViewType(position);
    }

    public Adapter<ViewHolder> getWrappedAdapter() {
        return this.mAdapter;
    }

    public long getItemId(int position) {
        return this.mAdapter.getItemId(position);
    }
}
