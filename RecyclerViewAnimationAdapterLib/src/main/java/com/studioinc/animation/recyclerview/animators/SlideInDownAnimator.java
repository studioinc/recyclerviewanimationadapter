//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.studioinc.animation.recyclerview.animators;

import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.animation.Interpolator;

public class SlideInDownAnimator extends BaseItemAnimator {
    public SlideInDownAnimator() {
    }

    public SlideInDownAnimator(Interpolator interpolator) {
        this.mInterpolator = interpolator;
    }

    protected void animateRemoveImpl(ViewHolder holder) {
        ViewCompat.animate(holder.itemView).translationY((float)(-holder.itemView.getHeight())).alpha(0.0F).setDuration(this.getRemoveDuration()).setInterpolator(this.mInterpolator).setListener(new DefaultRemoveVpaListener(holder)).setStartDelay(this.getRemoveDelay(holder)).start();
    }

    protected void preAnimateAddImpl(ViewHolder holder) {
        ViewCompat.setTranslationY(holder.itemView, (float)(-holder.itemView.getHeight()));
        ViewCompat.setAlpha(holder.itemView, 0.0F);
    }

    protected void animateAddImpl(ViewHolder holder) {
        ViewCompat.animate(holder.itemView).translationY(0.0F).alpha(1.0F).setDuration(this.getAddDuration()).setInterpolator(this.mInterpolator).setListener(new DefaultAddVpaListener(holder)).setStartDelay(this.getAddDelay(holder)).start();
    }
}
